# software_quiz
> Quiz platform for software engineers 

### Development setup

* Install and run Docker
* Build app `./script build_image`
* Run app in development mode `./script run_development`
* Open app on `http://localhost:8080`

### Production setup

* Install and run Docker
* Build app `./script build_image`
* Run app in production mode `./script run_production`
* Open app on `http://localhost:8080`

### Unit testing setup

* Install and run Docker
* Build app `./script build_image`
* Run unit tests via `./script run_units`

### E2E testing setup

* Install and run Docker
* Build app `./script build_image`
* Run app in production mode `./script run_production`
* Run e2e tests via `./script run_e2e`

### Other commands

* Build and run app in development mode `./script build_image run_development`
* Build and run app in production mode `./script build_image run_production`
* Build and run unit tests `./script build_image run_units`
