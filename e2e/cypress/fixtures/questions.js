export const JSQuestion1 = {
  type: 'JS',
  name: 'Some question',
  correctAnswer: 'Q1 correctAnswer',
  answer1: 'Q1 answer1',
  answer2: 'Q1 answer2',
  answer3: 'Q1 answer3'
}

export const JSQuestion2 = {
  type: 'JS',
  name: 'Other question',
  correctAnswer: 'Q2 correctAnswer',
  answer1: 'Q2 answer1',
  answer2: 'Q2 answer2',
  answer3: 'Q2 answer3'
}

export const JSQuestion3 = {
  type: 'JS',
  name: 'Edited question',
  correctAnswer: 'Q3 correctAnswer',
  answer1: 'Q3 answer1',
  answer2: 'Q3 answer2',
  answer3: 'Q3 answer3'
}

export const PHPQuestion1 = {
  type: 'PHP',
  name: 'PHP question',
  correctAnswer: 'Q3 correctAnswer',
  answer1: 'Q3 answer1',
  answer2: 'Q3 answer2',
  answer3: 'Q3 answer3'
}
