import Page from './Page'
import { admin } from '../fixtures/users'

class LoginPage extends Page {
  constructor(driver) {
    super(driver, '/login')
  }

  get inputEmail() {
    return this.driver.getByTestId('form-input-email')
  }

  get inputPassword() {
    return this.driver.getByTestId('form-input-password')
  }

  get buttonSubmit() {
    return this.driver.getByTestId('button-submit')
  }

  loginAsAdmin() {
    this.visit()
    this.inputEmail.type(admin.email)
    this.inputPassword.type(admin.password)
    this.buttonSubmit.click()
    this.driver.url().should('include', '/home')
  }

  verifyEmptyForm() {
    this.verifyEmptyInput(this.inputEmail)
    this.verifyEmptyInput(this.inputPassword)
  }

  verifyEmptyInput(input) {
    input.should('have.value', '')
  }
}

export default new LoginPage(cy)
