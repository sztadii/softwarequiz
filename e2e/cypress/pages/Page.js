export default class Page {
  constructor(driver, url = '/') {
    this.driver = driver
    this.url = url
  }

  visit() {
    this.driver.visit(this.url)
  }
}
