import Page from './Page'

class QuestionsPage extends Page {
  constructor(driver) {
    super(driver, '/questions')
  }

  verifyNewCreatedQuestion(question) {
    this.driver.getAllByTestId('details-card')
    this.driver.getByText(question.type)
    this.driver.getByText(question.name)
  }

  verifyHiddenQuestion(question) {
    this.driver
      .getAllByTestId('details-card')
      .contains(question.type)
      .should('have.length', '0')
  }

  clickTypeButton(type) {
    this.driver.getAllByTestId(`type-button-${type}`).click()
  }

  clickDetailsCard() {
    this.driver.getByTestId('details-card').click()
  }

  deleteFirstQuestion() {
    this.clickDetailsCard()
    this.driver.getByText('Delete').click()
  }

  deleteAllQuestions() {
    this.driver.getByText('Delete all').click()
  }

  disapproveAllQuestions() {
    this.driver.getByText('Disapprove all').click()
  }

  approveAllQuestions() {
    this.driver.getByText('Approve all').click()
  }

  verifyEmptyList() {
    this.driver.getByText('You do not have any questions :(')
  }

  clickQuestionCreationButton() {
    this.driver.getByText('Create first one').click()
  }
}

export default new QuestionsPage(cy)
