class AppPage {
  constructor(driver) {
    this.driver = driver
  }

  deleteAllQuestions() {
    this.driver.request('DELETE', '/api/questions')
  }
}

export default new AppPage(cy)
