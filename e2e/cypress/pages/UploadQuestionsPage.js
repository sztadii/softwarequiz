import Page from './Page'

class UploadQuestionsPage extends Page {
  constructor(driver) {
    super(driver, '/questions/upload')
  }

  uploadCSV() {
    this.driver.uploadFile('input[name=file]', 'questions.csv', 'text/csv')
  }

  verifyNewQuestionsList() {
    this.driver.getAllByTestId('list-item')
    this.driver.getAllByTestId('list-item').should('have.length', '2')
  }

  verifyErrorMessage() {
    this.driver.getAllByTestId('error-message')
  }
}

export default new UploadQuestionsPage(cy)
