import { JSQuestion1 } from '../fixtures/questions'
import Page from './Page'

class QuestionFormPage extends Page {
  constructor(driver) {
    super(driver, '/questions/form')
  }

  get inputType() {
    return this.driver.getByTestId('form-input-type')
  }

  get inputName() {
    return this.driver.getByTestId('form-input-name')
  }

  get inputAnswer0() {
    return this.driver.getByTestId('form-input-correctAnswer')
  }

  get inputAnswer1() {
    return this.driver.getByTestId('form-input-answer1')
  }

  get inputAnswer2() {
    return this.driver.getByTestId('form-input-answer2')
  }

  get inputAnswer3() {
    return this.driver.getByTestId('form-input-answer3')
  }

  get toggler() {
    return this.driver.getByTestId('toggler')
  }

  get buttonSubmit() {
    return this.driver.getByTestId('button-submit')
  }

  get successMessage() {
    return this.driver.getByTestId('success-message')
  }

  get errorMessage() {
    return this.driver.getByTestId('error-message')
  }

  fillForm(question = {}) {
    //TODO Fix problem with cut words during typing
    this.driver.wait(400)

    this.fillInput(this.inputType, question.type || JSQuestion1.type)
    this.fillInput(this.inputName, question.name || JSQuestion1.name)
    this.fillInput(this.inputAnswer0, question.correctAnswer || JSQuestion1.correctAnswer)
    this.fillInput(this.inputAnswer1, question.answer1 || JSQuestion1.answer1)
    this.fillInput(this.inputAnswer2, question.answer2 || JSQuestion1.answer2)
    this.fillInput(this.inputAnswer3, question.answer3 || JSQuestion1.answer3)
  }

  fillInput(input, value) {
    input.clear()
    input.type(value)
  }

  clickAvailabilityToggler() {
    this.toggler.click()
  }

  submitForm() {
    this.buttonSubmit.click()
  }

  verifyEmptyForm() {
    this.verifyEmptyInput(this.inputType)
    this.verifyEmptyInput(this.inputName)
    this.verifyEmptyInput(this.inputAnswer0)
    this.verifyEmptyInput(this.inputAnswer1)
    this.verifyEmptyInput(this.inputAnswer2)
    this.verifyEmptyInput(this.inputAnswer3)
  }

  verifyEmptyInput(input) {
    input.should('have.value', '')
  }

  verifySuccessMessage() {
    this.successMessage
  }

  verifyErrorMessage() {
    this.errorMessage
  }
}

export default new QuestionFormPage(cy)
