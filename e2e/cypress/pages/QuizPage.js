import { JSQuestion1, JSQuestion2 } from '../fixtures/questions'
import Page from './Page'

class QuizPage extends Page {
  constructor(driver) {
    super(driver)
  }

  verifyStartedQuiz() {
    this.driver.getByTestId('quiz-card')
  }

  chooseCorrectAnswers() {
    this.driver.getByText(JSQuestion1.correctAnswer).click()
    this.driver.getByText(JSQuestion2.correctAnswer).click()
  }

  chooseIncorrectAnswers() {
    this.driver.getByText(JSQuestion1.answer1).click()
    this.driver.getByText(JSQuestion2.answer1).click()
  }

  submitQuiz() {
    this.driver.getByText('Submit').click()
  }

  verifyHighestScore() {
    this.driver.getByText(/Your score: 2/)
  }

  verifyLowestScore() {
    this.driver.getByText(/Your score: 0/)
  }
}

export default new QuizPage(cy)
