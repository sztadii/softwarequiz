import Page from './Page'

class HomePage extends Page {
  constructor(driver) {
    super(driver, '/home')
  }

  verifyEmptyQuizMessage() {
    this.driver.getByText('You do not have any questions :(')
  }

  clickFirstQuiz() {
    this.driver.getByTestId('link-to-quiz').click({ force: true })
  }

  clickRandomTypeQuiz() {
    this.driver.getByText('Random').click({ force: true })
  }
}

export default new HomePage(cy)
