import app from '../pages/AppPage'
import login from '../pages/LoginPage'
import questionForm from '../pages/QuestionFormPage'
import questions from '../pages/QuestionsPage'
import home from '../pages/HomePage'
import quiz from '../pages/QuizPage'
import { JSQuestion1, JSQuestion3, PHPQuestion1 } from '../fixtures/questions'

describe('Questions', () => {
  beforeEach(() => {
    login.loginAsAdmin()
    app.deleteAllQuestions()
    questions.visit()
  })

  it('when user visit questions with empty list then we will able to click into button and redirect to creation form', () => {
    questions.verifyEmptyList()
    questions.clickQuestionCreationButton()
    questionForm.verifyEmptyForm()
  })

  it('when user create question then he will able to see it on questions page', () => {
    createAndVerifyQuestion()
  })

  it('when user create question then he is able to remove it from questions page', () => {
    createAndVerifyQuestion()

    questions.deleteFirstQuestion()
    questions.verifyEmptyList()
  })

  it('when user create question then he is able to make it unavailable', () => {
    questions.clickQuestionCreationButton()
    questionForm.fillForm(JSQuestion1)
    questionForm.clickAvailabilityToggler()
    questionForm.submitForm()
    questions.visit()
    questions.verifyNewCreatedQuestion(JSQuestion1)
    home.visit()
    home.verifyEmptyQuizMessage()
  })

  it('when user create question then he will able to delete all by one click', () => {
    createAndVerifyQuestion()

    questions.deleteAllQuestions()
    questions.verifyEmptyList()
  })

  it('when user create question then he will able to disapprove and approve all questions by one click', () => {
    createAndVerifyQuestion()

    questions.disapproveAllQuestions()
    home.visit()
    home.verifyEmptyQuizMessage()
    questions.visit()
    questions.approveAllQuestions()
    home.visit()
    home.clickFirstQuiz()
    quiz.verifyStartedQuiz()
  })

  it('when user create question then he is able to edit it', () => {
    createAndVerifyQuestion()

    questions.clickDetailsCard()
    questionForm.fillForm(JSQuestion3)
    questionForm.submitForm()
    questions.visit()
    questions.verifyNewCreatedQuestion(JSQuestion3)
  })

  it('when user create two question ( with different types ) then he is able to filter it on questions page', () => {
    questions.clickQuestionCreationButton()
    questionForm.fillForm(JSQuestion1)
    questionForm.submitForm()
    questionForm.fillForm(PHPQuestion1)
    questionForm.submitForm()
    questions.visit()
    questions.verifyNewCreatedQuestion(JSQuestion1)
    questions.verifyNewCreatedQuestion(PHPQuestion1)
    questions.clickTypeButton(JSQuestion1.type)
    questions.verifyNewCreatedQuestion(JSQuestion1)
    questions.verifyHiddenQuestion(PHPQuestion1)
    questions.clickTypeButton(JSQuestion1.type)
    questions.verifyNewCreatedQuestion(JSQuestion1)
    questions.verifyNewCreatedQuestion(PHPQuestion1)
  })
})

function createAndVerifyQuestion() {
  questions.clickQuestionCreationButton()
  questionForm.fillForm(JSQuestion1)
  questionForm.submitForm()
  questions.visit()
  questions.verifyNewCreatedQuestion(JSQuestion1)
}
