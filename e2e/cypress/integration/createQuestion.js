import app from '../pages/AppPage'
import login from '../pages/LoginPage'
import questionForm from '../pages/QuestionFormPage'

describe('Create question', () => {
  beforeEach(() => {
    login.loginAsAdmin()
    app.deleteAllQuestions()
    questionForm.visit()
  })

  it('when user visit questionForm page then he will see empty form', () => {
    questionForm.verifyEmptyForm()
  })

  it('when user fill question form correctly then he will see success message', () => {
    questionForm.fillForm()
    questionForm.submitForm()
    questionForm.verifySuccessMessage()
  })

  it('when user submit empty form then he will see error message', () => {
    questionForm.submitForm()
    questionForm.verifyErrorMessage()
  })
})
