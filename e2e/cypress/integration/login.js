import app from '../pages/AppPage'
import login from '../pages/LoginPage'
import questionForm from '../pages/QuestionFormPage'
import uploadQuestions from '../pages/UploadQuestionsPage'
import questions from '../pages/QuestionsPage'
import { JSQuestion1 } from '../fixtures/questions'

describe('Login', () => {
  beforeEach(() => {
    app.deleteAllQuestions()
    questions.visit()
  })

  it('when user create one question and try to update it then he will see login page', () => {
    questions.clickQuestionCreationButton()
    questionForm.fillForm(JSQuestion1)
    questionForm.submitForm()
    questions.visit()
    questions.verifyNewCreatedQuestion(JSQuestion1)
    questions.clickDetailsCard()
    login.verifyEmptyForm()
  })

  it('when user is trying to upload questions then he will see login page', () => {
    uploadQuestions.visit()
    uploadQuestions.uploadCSV()
    login.verifyEmptyForm()
    questions.visit()
    questions.verifyEmptyList()
  })
})
