import app from '../pages/AppPage'
import login from '../pages/LoginPage'
import home from '../pages/HomePage'
import questionForm from '../pages/QuestionFormPage'
import quiz from '../pages/QuizPage'
import { JSQuestion1, JSQuestion2, PHPQuestion1 } from '../fixtures/questions'

describe('Quiz', () => {
  beforeEach(() => {
    login.loginAsAdmin()
    app.deleteAllQuestions()
    home.visit()
  })

  it('when user visit home page then he will see message about empty list', () => {
    home.verifyEmptyQuizMessage()
  })

  it('when user create few questions then he is able to start random quiz', () => {
    createQuestionsAndVisitHomePage()

    home.clickRandomTypeQuiz()
    quiz.verifyStartedQuiz()
    quiz.submitQuiz()
    quiz.verifyLowestScore()
  })

  it('when user create few questions and choose choose correct answers when he will be able to finish quiz with highest score', () => {
    createQuestionsAndStartQuiz()

    quiz.chooseCorrectAnswers()
    quiz.submitQuiz()
    quiz.verifyHighestScore()
  })

  it('when user create few questions and choose incorrect answers when he will be able to finish quiz with lowest score', () => {
    createQuestionsAndStartQuiz()

    quiz.chooseIncorrectAnswers()
    quiz.submitQuiz()
    quiz.verifyLowestScore()
  })

  it('when user create few questions and do not choose any answers when he will be able to finish quiz with lowest score', () => {
    createQuestionsAndStartQuiz()

    quiz.submitQuiz()
    quiz.verifyLowestScore()
  })
})

function createQuestionsAndVisitHomePage() {
  home.verifyEmptyQuizMessage()
  questionForm.visit()
  questionForm.fillForm(JSQuestion1)
  questionForm.submitForm()
  questionForm.fillForm(JSQuestion2)
  questionForm.submitForm()
  questionForm.fillForm(PHPQuestion1)
  questionForm.submitForm()
  home.visit()
}

function createQuestionsAndStartQuiz() {
  createQuestionsAndVisitHomePage()
  home.clickFirstQuiz()
  quiz.verifyStartedQuiz()
}
