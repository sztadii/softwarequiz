import app from '../pages/AppPage'
import uploadQuestions from '../pages/UploadQuestionsPage'
import questions from '../pages/QuestionsPage'
import login from '../pages/LoginPage'

describe('Upload questions', () => {
  beforeEach(() => {
    login.loginAsAdmin()
  })

  it('when user visit upload questions page then he will be able to upload new questions via CSV', () => {
    app.deleteAllQuestions()
    uploadQuestions.visit()
    uploadQuestions.uploadCSV()
    uploadQuestions.verifyNewQuestionsList()
    questions.visit()

    const uploadedQuestions = [
      {
        type: 'JS',
        name: 'What is this?'
      },
      {
        type: 'PHP',
        name: 'Hmm?'
      }
    ]

    uploadedQuestions.forEach(q => {
      questions.verifyNewCreatedQuestion(q)
    })
  })

  it('when user want to upload questions which already exists then he will get error', () => {
    uploadQuestions.visit()
    uploadQuestions.uploadCSV()
    uploadQuestions.verifyErrorMessage()
  })
})
