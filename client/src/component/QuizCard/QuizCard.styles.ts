import styled from 'styled-components'

export const Top = styled.div`
  margin-bottom: 10px;
  padding-bottom: 10px;
  border-bottom: 1px solid ${props => props.theme.border};
`

export const Answer = styled.div`
  margin-bottom: 10px;

  &:last-child {
    margin-bottom: 0;
  }
`
