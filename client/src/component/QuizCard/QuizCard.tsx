import * as React from 'react'
import Card from '../Card/Card'
import { QuizQuestion } from '../../type/Quiz'
import Button from '../Button/Button'
import { Top, Answer } from './QuizCard.styles'

interface DetailsCardProps {
  quizQuestion: QuizQuestion
  onAnswerSubmit: (question: QuizQuestion, answer: string) => void
}

export default class QuizCard extends React.Component<DetailsCardProps> {
  render() {
    const { quizQuestion, onAnswerSubmit } = this.props
    const { chosenAnswer } = quizQuestion
    return (
      <Card data-testid="quiz-card">
        <Top>{quizQuestion.name}</Top>
        {quizQuestion.answers.map(answer => {
          const activeClass = chosenAnswer === answer ? 'is-active' : ''
          return (
            <Answer key={answer}>
              <Button
                className={activeClass}
                onClick={() => {
                  onAnswerSubmit(quizQuestion, answer)
                }}
              >
                {answer}
              </Button>
            </Answer>
          )
        })}
      </Card>
    )
  }
}
