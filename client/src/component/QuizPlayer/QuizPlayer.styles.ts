import styled from 'styled-components'

export const Message = styled.div`
  color: ${props => props.theme.background};
  border-radius: ${props => props.theme.baseRadius};
  padding: 10px 20px;
  box-shadow: 0 5px 20px ${props => props.theme.shadow};
  background: ${props => props.theme.main};
`
