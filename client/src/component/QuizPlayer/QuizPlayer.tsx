import * as React from 'react'
import { observer } from 'mobx-react'
import { Question } from '../../type/Question'
import { QuizQuestion } from '../../type/Quiz'
import QuizCard from '../QuizCard/QuizCard'
import Button from '../Button/Button'
import Line from '../Line/Line'
import { Message } from './QuizPlayer.styles'

interface QuizPlayerState {
  quiz: QuizQuestion[]
  score: number
}

interface QuizPlayerProps {
  questions: Question[]
  onFinish: () => void
}

@observer
export default class QuizPlayer extends React.Component<QuizPlayerProps, QuizPlayerState> {
  constructor(props: QuizPlayerProps) {
    super(props)
    this.state = {
      quiz: props.questions,
      score: null
    }
  }

  static getDerivedStateFromProps(props: QuizPlayerProps) {
    return {
      quiz: props.questions
    }
  }

  handleAnswerSubmit = (quizQuestion: QuizQuestion, answer: string) => {
    const { quiz } = this.state
    const index = quiz.findIndex(q => q._id === quizQuestion._id)

    if (index >= 0) {
      quiz[index].chosenAnswer = answer
    }

    this.setState({
      quiz
    })
  }

  handleQuizSubmit = () => {
    const score = this.getQuizScore()
    this.setState({ score })
    this.props.onFinish()
  }

  getQuizScore = () => {
    const { quiz } = this.state
    return quiz.reduce((sum, question) => {
      return question.correctAnswer === question.chosenAnswer ? sum + 1 : sum
    }, 0)
  }

  renderContent = () => {
    const { score, quiz } = this.state
    return (
      <div>
        {!score &&
          quiz.map(quizQuestion => {
            return (
              <QuizCard
                onAnswerSubmit={this.handleAnswerSubmit}
                quizQuestion={quizQuestion}
                key={quizQuestion._id}
              />
            )
          })}

        <Button onClick={this.handleQuizSubmit}>Submit</Button>
      </div>
    )
  }

  renderScore = () => {
    const { score } = this.state
    return (
      <div>
        <Line />
        <Message>Your score: {score}</Message>
      </div>
    )
  }

  render() {
    const { score } = this.state
    return <div>{score === null ? this.renderContent() : this.renderScore()}</div>
  }
}
