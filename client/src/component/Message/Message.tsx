import styled from 'styled-components'

export default styled.div`
  color: ${props => props.theme.background};
  border-radius: ${props => props.theme.baseRadius};
  padding: 10px 20px;
  box-shadow: 0 5px 20px ${props => props.theme.shadow};
  margin-bottom: 20px;
  animation: messageStart 0.4s;

  @keyframes messageStart {
    from {
      opacity: 0;
      transform: translateY(10px);
    }
    to {
      opacity: 1;
      transform: translateY(0);
    }
  }

  &.error {
    background: ${props => props.theme.error};
  }

  &.success {
    background: ${props => props.theme.success};
  }
`
