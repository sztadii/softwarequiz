import * as React from 'react'
import { RouteComponentProps, withRouter } from 'react-router-dom'
import Title from '../Title/Title'
import Card from '../Card/Card'
import Button from '../Button/Button'
import FormInput from '../FormInput/FormInput'
import * as UserService from '../../service/UserService'
import Message from '../Message/Message'

interface LoginFormState {
  email: string
  password: string
  success: string
  error: string
}

class LoginForm extends React.Component<RouteComponentProps> {
  initState: LoginFormState = {
    email: '',
    password: '',
    success: '',
    error: ''
  }

  state = {
    ...this.initState
  }

  handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const value = e.target.value
    const name = e.target.name
    this.setState({ [name]: value, error: '', success: '' })
  }

  handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault()
    const { email, password } = this.state

    const isEmptyForm = ![email, password].every(e => !!e)

    if (isEmptyForm) {
      this.displayError('Your form can not be empty')
      return
    }

    try {
      await UserService.authenticateUser({ email, password })
      this.props.history.push('/')
    } catch (e) {
      this.displayError(e.message)
    }
  }

  displayError = (error: string) => this.setState({ error })

  render() {
    const { email, password, error } = this.state

    return (
      <form onSubmit={this.handleSubmit} data-testid="create-form">
        <Title>Login</Title>
        <Card>
          {error && (
            <Message className="error" data-testid="error-message">
              {error}
            </Message>
          )}

          <FormInput
            value={email}
            onChange={this.handleChange}
            name="email"
            type="text"
            label="Email"
          />
          <FormInput
            value={password}
            onChange={this.handleChange}
            name="password"
            type="password"
            label="Password"
          />

          <Button data-testid="button-submit">Login</Button>
        </Card>
      </form>
    )
  }
}

// Bellow message from: 'react-router type definitions`
// There is a known issue in TypeScript, which doesn't allow decorators to change the signature of the classes
export default withRouter(LoginForm)
