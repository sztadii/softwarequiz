import * as React from 'react'
import Title from '../Title/Title'
import Card from '../Card/Card'
import UploadButton from '../UploadButton/UploadButton'
import Message from '../Message/Message'
import { uploadQuestions } from '../../service/QuestionService'
import { Question } from '../../type/Question'
import ListItem from '../ListItem/ListItem'
import { Space } from './UploadForm.styles'

interface UploadFormState {
  error: string
  loading: boolean
  questions: Question[]
}

export default class UploadForm extends React.Component<{}, UploadFormState> {
  state: UploadFormState = {
    error: '',
    loading: false,
    questions: []
  }

  handleFileUpload = async (e: React.ChangeEvent<HTMLInputElement>) => {
    try {
      const files = Array.from(e.target.files)

      this.setState({ loading: true })

      const formData = new FormData()

      files.forEach(file => {
        formData.append('files', file)
      })

      const questions = await uploadQuestions(formData)

      this.setState({ loading: false, questions })
    } catch (e) {
      this.setState({ error: e.message })
    }
  }

  render() {
    const { questions, error } = this.state
    return (
      <div>
        <Title>Upload questions</Title>
        <Card>
          {error && (
            <Message data-testid="error-message" className="error">
              {error}
            </Message>
          )}

          <UploadButton onChange={this.handleFileUpload} />

          {!!questions.length && (
            <Space>
              {questions.map(question => (
                <ListItem key={question._id} name="Name" value={question.name} />
              ))}
            </Space>
          )}
        </Card>
      </div>
    )
  }
}
