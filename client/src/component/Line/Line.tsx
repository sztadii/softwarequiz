import styled from 'styled-components'

export default styled.div`
  background: ${props => props.theme.border};
  height: 1px;
  margin: 20px 0;
`
