import * as React from 'react'
import { Link } from 'react-router-dom'
import { observer } from 'mobx-react'
import PlayItem from '../PlayItem/PlayItem'

interface QuizListProps {
  types: string[]
}

@observer
export default class QuizList extends React.Component<QuizListProps> {
  render() {
    const { types } = this.props
    return (
      <div>
        {types.map(type => {
          return (
            <Link to={`quiz/?type=${type}`} key={type} data-testid="link-to-quiz">
              <PlayItem value={type} />
            </Link>
          )
        })}

        {types.length > 1 && (
          <Link to="quiz/" data-testid="link-to-quiz">
            <PlayItem value="Random" />
          </Link>
        )}
      </div>
    )
  }
}
