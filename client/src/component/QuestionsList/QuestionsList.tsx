import * as React from 'react'
import { observer } from 'mobx-react'
import { Link } from 'react-router-dom'
import { Question } from '../../type/Question'
import DetailsCard from '../DetailsCard/DetailsCard'

interface QuestionsListProps {
  questions: Question[]
}

@observer
export default class QuestionsList extends React.Component<QuestionsListProps> {
  render() {
    const { questions } = this.props

    return (
      <div>
        {questions.map(question => {
          const array = [
            {
              name: 'Type',
              value: question.type
            },
            {
              name: 'Name',
              value: question.name
            },
            {
              name: 'Is approved',
              value: question.isApproved ? 'Yes' : 'No'
            }
          ]
          const { _id } = question
          return (
            <Link key={_id} to={`/questions/form?id=${_id}`}>
              <DetailsCard array={array} />
            </Link>
          )
        })}
      </div>
    )
  }
}
