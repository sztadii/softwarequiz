import * as React from 'react'
import { Wrapper, Toggle } from './Toggler.styles'
import Label from '../Label/Label'

interface TogglerProps {
  label?: string
  defaultChecked?: boolean
  onChange: (value: boolean) => void
}

interface TogglerState {
  isActive: boolean
}

export default class Toggler extends React.Component<TogglerProps, TogglerState> {
  state = {
    isActive: this.props.defaultChecked
  }

  static getDerivedStateFromProps(props: TogglerProps) {
    return {
      isActive: props.defaultChecked
    }
  }

  handleToggle = () => {
    const { onChange } = this.props
    const isActive = !this.state.isActive
    this.setState({
      isActive
    })
    onChange(isActive)
  }

  render() {
    const { label } = this.props
    const { isActive } = this.state
    const toggleActiveClass = isActive ? 'is-active' : ''
    return (
      <div>
        {label && <Label>{label}</Label>}
        <Wrapper onClick={this.handleToggle} data-testid="toggler">
          <Toggle className={toggleActiveClass} />
        </Wrapper>
      </div>
    )
  }
}
