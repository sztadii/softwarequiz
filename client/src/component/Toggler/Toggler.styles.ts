import styled from 'styled-components'

export const Wrapper = styled.div`
  position: relative;
  height: 32px;
  width: 60px;
  border: 1px solid ${props => props.theme.border};
  border-radius: ${props => props.theme.baseRadius};
  cursor: pointer;
  margin-bottom: 20px;

  &:before,
  &:after {
    position: absolute;
    top: 50%;
    width: 50%;
    font-size: 10px;
    transform: translateY(-50%);
    text-align: center;
  }

  &:before {
    content: 'ON';
    left: 0;
  }

  &:after {
    content: 'OFF';
    right: 0;
  }
`

export const Toggle = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  height: 30px;
  width: 30px;
  border-radius: ${props => props.theme.baseRadius};
  background: ${props => props.theme.dark};
  transition: 0.3s;
  z-index: 1;

  &.is-active {
    transform: translateX(100%);
  }
`
