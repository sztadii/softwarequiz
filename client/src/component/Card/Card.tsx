import styled from 'styled-components'

export default styled.div`
  padding: 20px;
  box-shadow: 0 5px 20px ${props => props.theme.shadow};
  border-radius: ${props => props.theme.baseRadius};
  margin-bottom: 20px;
  position: relative;
  overflow: hidden;

  &:after {
    content: '';
    position: absolute;
    bottom: 0;
    left: 0;
    width: 100%;
    height: 8px;
    background: ${props => props.theme.main};
  }
`
