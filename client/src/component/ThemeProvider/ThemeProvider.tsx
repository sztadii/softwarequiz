import * as React from 'react'
import { ThemeProvider as Provider } from 'styled-components'
import theme from '../../config/theme'

interface ThemeProviderProps {
  children: React.ReactChild
}

export default class ThemeProvider extends React.Component<ThemeProviderProps> {
  render() {
    return <Provider theme={theme}>{this.props.children}</Provider>
  }
}
