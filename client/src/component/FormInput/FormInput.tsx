import * as React from 'react'
import Label from '../Label/Label'
import { Input } from './FormInput.styles'

interface FormInputProps {
  name: string
  type: string
  label: string
  value: string | number
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void
}

export default class FormInput extends React.Component<FormInputProps> {
  render() {
    const { name, type, label, value, onChange } = this.props
    return (
      <div>
        <Label>{label}</Label>
        <Input
          value={value}
          onChange={onChange}
          name={name}
          type={type}
          data-testid={`form-input-${name}`}
        />
      </div>
    )
  }
}
