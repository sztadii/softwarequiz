import styled from 'styled-components'

export const Input = styled.input`
  display: block;
  width: 100%;
  padding: 10px 20px;
  margin-bottom: 20px;
  border: 1px solid ${props => props.theme.border};
  border-radius: ${props => props.theme.baseRadius};
  font-size: 15px;
`
