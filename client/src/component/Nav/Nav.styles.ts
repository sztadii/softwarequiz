import styled from 'styled-components'

export const Box = styled.nav`
  display: flex;
  align-items: center;
  justify-content: center;
  position: fixed;
  top: 0;
  left: 0;
  height: 70px;
  width: 100%;
  padding: 20px;
  background: ${props => props.theme.main};
  color: ${props => props.theme.background};
  box-shadow: 0 5px 20px ${props => props.theme.shadow};
  z-index: 10;
  transition: 0.7s;

  &.is-disabled {
    background: ${props => props.theme.dark};
    pointer-events: none;
  }
`

export const BoxSpace = styled.nav`
  height: 70px;
  margin-bottom: 20px;
`

export const Item = styled.div`
  text-transform: uppercase;
  padding: 20px;
  font-size: 25px;
  transition: 0.7s;

  &.is-disabled {
    opacity: 0.3;
  }
`
