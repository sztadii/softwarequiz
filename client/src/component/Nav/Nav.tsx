import * as React from 'react'
import { NavLink } from 'react-router-dom'
import { inject, observer } from 'mobx-react'
import { MdDashboard, MdLibraryBooks, MdLibraryAdd, MdBackup } from 'react-icons/md'
import { Box, BoxSpace, Item } from './Nav.styles'
import QuestionStore from '../../store/QuestionStore'

interface NavProps {
  store?: QuestionStore
}

@inject('store')
@observer
export default class Nav extends React.Component<NavProps> {
  render() {
    const { isActiveQuiz } = this.props.store
    const disabledClass = isActiveQuiz ? 'is-disabled' : ''
    return (
      <div>
        <Box className={disabledClass}>
          <NavLink to="/">
            <Item className={disabledClass}>
              <MdDashboard />
            </Item>
          </NavLink>
          <NavLink to="/questions">
            <Item className={disabledClass}>
              <MdLibraryBooks />
            </Item>
          </NavLink>
          <NavLink to="/questions/form">
            <Item className={disabledClass}>
              <MdLibraryAdd />
            </Item>
          </NavLink>
          <NavLink to="/questions/upload">
            <Item className={disabledClass}>
              <MdBackup />
            </Item>
          </NavLink>
        </Box>
        <BoxSpace />
      </div>
    )
  }
}
