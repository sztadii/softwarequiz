import styled from 'styled-components'

export default styled.button`
  text-decoration: none;
  text-transform: uppercase;
  color: ${props => props.theme.background};
  box-shadow: 0 5px 20px ${props => props.theme.shadow};
  background: ${props => props.theme.main};
  border-radius: ${props => props.theme.largeRadius};
  border: none;
  padding: 10px 20px;
  text-align: center;
  outline: none;
  cursor: pointer;
  transition: 0.3s;

  &.is-active {
    background: ${props => props.theme.dark};
  }
`
