import * as React from 'react'
import QuestionForm from './QuestionForm'
import { cleanup, wait } from 'react-testing-library'
import { fireInputUpdate, renderWithRouterAndStore } from '../../utils/testUtils'

jest.mock('../../service/QuestionService', () => ({
  createQuestion: jest.fn().mockImplementationOnce(() => {})
}))
afterEach(cleanup)

function renderComponent() {
  return renderWithRouterAndStore(<QuestionForm />)
}

test('Form display error when form is empty', () => {
  const { getByText, getByTestId } = renderComponent()
  const button = getByTestId('button-submit')
  button.click()
  getByText('Your form can not be empty')
})

test('Form display error when answers are not uniq', () => {
  const { getByText, getByTestId } = renderComponent()
  const button = getByTestId('button-submit')
  const inputType = getByTestId('form-input-type') as HTMLInputElement
  const inputName = getByTestId('form-input-name') as HTMLInputElement
  const inputAnswer0 = getByTestId('form-input-correctAnswer') as HTMLInputElement
  const inputAnswer1 = getByTestId('form-input-answer1') as HTMLInputElement
  const inputAnswer2 = getByTestId('form-input-answer2') as HTMLInputElement
  const inputAnswer3 = getByTestId('form-input-answer3') as HTMLInputElement

  fireInputUpdate(inputType, 'JS')
  fireInputUpdate(inputName, 'What is CORS?')
  fireInputUpdate(inputAnswer0, 'Cross origin resource sharing')
  fireInputUpdate(inputAnswer1, 'Nothing')
  fireInputUpdate(inputAnswer2, 'Nothing')
  fireInputUpdate(inputAnswer3, 'I do not know')

  button.click()

  getByText('You do not have uniq answers')
})

test('Form questionForm new questions without any errors', async () => {
  const { getByText, getByTestId } = renderComponent()
  const button = getByTestId('button-submit')
  const inputType = getByTestId('form-input-type') as HTMLInputElement
  const inputName = getByTestId('form-input-name') as HTMLInputElement
  const inputAnswer0 = getByTestId('form-input-correctAnswer') as HTMLInputElement
  const inputAnswer1 = getByTestId('form-input-answer1') as HTMLInputElement
  const inputAnswer2 = getByTestId('form-input-answer2') as HTMLInputElement
  const inputAnswer3 = getByTestId('form-input-answer3') as HTMLInputElement

  fireInputUpdate(inputType, 'JS')
  fireInputUpdate(inputName, 'What is CORS?')
  fireInputUpdate(inputAnswer0, 'Cross origin resource sharing')
  fireInputUpdate(inputAnswer1, 'Nothing')
  fireInputUpdate(inputAnswer2, 'Chris will know it')
  fireInputUpdate(inputAnswer3, 'I do not know')

  button.click()

  await wait(() => {
    getByText('You have successfully created a question')
  })
})
