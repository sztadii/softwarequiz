import * as React from 'react'
import { inject } from 'mobx-react'
import { withRouter, RouteComponentProps } from 'react-router-dom'
import Title from '../Title/Title'
import Card from '../Card/Card'
import Button from '../Button/Button'
import FormInput from '../FormInput/FormInput'
import * as QuestionService from '../../service/QuestionService'
import getSearchParam from '../../utils/getSearchParam'
import { isAdmin } from '../../utils/authUtils'
import QuestionStore from '../../store/QuestionStore'
import Message from '../Message/Message'
import Toggler from '../Toggler/Toggler'
import Space from '../Space/Space'

interface QuestionsPageProps extends RouteComponentProps {
  store?: QuestionStore
}

interface QuestionFormState {
  error: string
  success: string
  type: string
  name: string
  correctAnswer: string
  answer1: string
  answer2: string
  answer3: string
  isApproved: boolean
}

@inject('store')
class QuestionForm extends React.Component<QuestionsPageProps> {
  initState: QuestionFormState = {
    error: '',
    success: '',
    type: '',
    name: '',
    correctAnswer: '',
    answer1: '',
    answer2: '',
    answer3: '',
    isApproved: isAdmin()
  }

  state = {
    ...this.initState
  }

  async componentDidMount() {
    const id = this.getQuestionId()

    if (id) {
      const question = await QuestionService.getQuestionById(id)
      this.setState({
        ...question,
        answer1: question.answers[1],
        answer2: question.answers[2],
        answer3: question.answers[3]
      })
    }
  }

  handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const value = e.target.value
    const name = e.target.name
    this.setState({ [name]: value, error: '', success: '' })
  }

  handleTogglerChange = (value: boolean) => {
    this.setState({ isApproved: value })
  }

  handleQuestionDelete = async () => {
    const { deleteQuestion } = this.props.store
    await deleteQuestion(this.getQuestionId())
    this.props.history.push('/questions')
  }

  fillRandom = () => {
    const randomNumber = Math.round(Math.random() * 1000)
    this.setState({
      type: 'Random type',
      name: `Random question with id ${randomNumber}`,
      correctAnswer: '1',
      answer1: '2',
      answer2: '3',
      answer3: '4'
    })
  }

  handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault()
    const { type, name, correctAnswer, answer1, answer2, answer3, isApproved } = this.state
    const answers = [correctAnswer, answer1, answer2, answer3]

    const isUniqAnswers = new Set(answers).size === answers.length
    const isEmptyForm = ![type, name, correctAnswer, answer1, answer2, answer3].every(e => !!e)

    if (isEmptyForm) {
      await this.displayError('Your form can not be empty')
      return
    }

    if (!isUniqAnswers) {
      await this.displayError('You do not have uniq answers')
      return
    }

    try {
      const _id = this.getQuestionId()

      const question = {
        type,
        name,
        answers,
        correctAnswer,
        isApproved
      }

      if (_id) {
        await QuestionService.updateQuestion({ _id, ...question })
      } else {
        await QuestionService.createQuestion(question)
        this.resetState()
      }

      await this.displaySuccess(`You have successfully ${_id ? 'updated' : 'created'} a question`)
    } catch (e) {
      await this.displayError(e.message)
    }
  }

  handleBack = () => this.props.history.goBack()

  getQuestionId = () => getSearchParam('id')

  displaySuccess = (success: string) => this.setState({ success })

  displayError = (error: string) => this.setState({ error })

  resetState = () => this.setState({ ...this.initState })

  render() {
    const {
      error,
      success,
      type,
      name,
      correctAnswer,
      answer1,
      answer2,
      answer3,
      isApproved
    } = this.state
    const isEditMode = this.getQuestionId()
    const hasAdminPermission = isAdmin()

    return (
      <form onSubmit={this.handleSubmit} data-testid="create-form">
        <Title>{isEditMode ? 'Edit question' : 'Create question'}</Title>
        <Card>
          {error && (
            <Message className="error" data-testid="error-message">
              {error}
            </Message>
          )}
          {success && (
            <Message className="success" data-testid="success-message">
              {success}
            </Message>
          )}

          <FormInput
            value={type}
            onChange={this.handleChange}
            name="type"
            type="text"
            label="Type"
          />
          <FormInput
            value={name}
            onChange={this.handleChange}
            name="name"
            type="text"
            label="Name"
          />
          <FormInput
            value={correctAnswer}
            onChange={this.handleChange}
            name="correctAnswer"
            type="text"
            label="Correct answer"
          />
          <FormInput
            value={answer1}
            onChange={this.handleChange}
            name="answer1"
            type="text"
            label="Other answer"
          />
          <FormInput
            value={answer2}
            onChange={this.handleChange}
            name="answer2"
            type="text"
            label="Other answer"
          />
          <FormInput
            value={answer3}
            onChange={this.handleChange}
            name="answer3"
            type="text"
            label="Other answer"
          />
          {hasAdminPermission && (
            <Toggler
              label="Is available"
              defaultChecked={isApproved}
              onChange={this.handleTogglerChange}
            />
          )}

          <Space>
            <Button data-testid="button-submit">{isEditMode ? 'Update' : 'Create'}</Button>
            <Button className="is-active" onClick={this.handleBack}>
              Cancel
            </Button>
            {isEditMode && (
              <Button type="button" onClick={this.handleQuestionDelete} className="is-active">
                Delete
              </Button>
            )}
            {!isEditMode && hasAdminPermission && (
              <Button type="button" onClick={this.fillRandom}>
                Fill random
              </Button>
            )}
          </Space>
        </Card>
      </form>
    )
  }
}

// Bellow message from: 'react-router type definitions`
// There is a known issue in TypeScript, which doesn't allow decorators to change the signature of the classes
export default withRouter(QuestionForm)
