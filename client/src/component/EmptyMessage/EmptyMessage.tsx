import * as React from 'react'
import { Link } from 'react-router-dom'
import Title from '../Title/Title'
import Button from '../Button/Button'

export default class EmptyMessage extends React.Component {
  render() {
    return (
      <div>
        <Title>You do not have any questions :(</Title>
        <Link to="/questions/form">
          <Button>Create first one</Button>
        </Link>
      </div>
    )
  }
}
