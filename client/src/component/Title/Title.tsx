import styled from 'styled-components'

export default styled.div`
  font-size: 24px;
  text-transform: uppercase;
  padding-left: 20px;
  position: relative;
  margin-bottom: 20px;

  &:before {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    width: 8px;
    height: 100%;
    background: ${props => props.theme.main};
    border-radius: ${props => props.theme.baseRadius};
  }
`
