import styled from 'styled-components'

export const Item = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 5px;
  padding: 5px 0;
  border-bottom: 1px solid ${props => props.theme.border};

  &:last-child {
    border-bottom: none;
  }
`

export const Title = styled.div`
  text-transform: uppercase;
  font-weight: bold;
  margin-right: 10px;
  min-width: 100px;
`
