import * as React from 'react'
import { Title, Item } from './ListItem.styles'

interface ListItemProps {
  name: string
  value: string | number | boolean
}

export default class ListItem extends React.Component<ListItemProps> {
  render() {
    const { name, value } = this.props
    return (
      <Item data-testid="list-item">
        <Title>{name}</Title>
        <div>{value}</div>
      </Item>
    )
  }
}
