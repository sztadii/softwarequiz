import * as React from 'react'

interface UploadButtonProps {
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void
}

export default function UploadButton(props: UploadButtonProps) {
  return (
    <div>
      <input type="file" name="file" onChange={props.onChange} />
    </div>
  )
}
