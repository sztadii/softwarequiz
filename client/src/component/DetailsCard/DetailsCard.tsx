import * as React from 'react'
import Card from '../Card/Card'
import ListItem from '../ListItem/ListItem'

interface DetailsCardProps {
  array: Array<{ name: string; value: string }>
}

export default class DetailsCard extends React.Component<DetailsCardProps> {
  render() {
    const { array } = this.props
    return (
      <Card data-testid="details-card">
        {array.map(i => (
          <ListItem key={i.name} name={i.name} value={i.value} />
        ))}
      </Card>
    )
  }
}
