import styled from 'styled-components'

export const Item = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 5px;
  padding: 5px 0;
  position: relative;
`

export const Title = styled.div`
  text-transform: uppercase;
  font-weight: bold;
  margin-right: 10px;
  min-width: 100px;
`

export const ButtonBox = styled.div`
  position: absolute;
  right: 5px;
  top: 50%;
  transform: translateY(-50%);
`
