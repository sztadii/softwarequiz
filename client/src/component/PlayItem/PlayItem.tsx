import * as React from 'react'
import Card from '../Card/Card'
import Button from '../Button/Button'
import { Item, Title, ButtonBox } from './PlayItem.styles'

interface PlayItemProps {
  value: string
}

export default class PlayItem extends React.Component<PlayItemProps> {
  render() {
    const { value } = this.props
    return (
      <Card data-testid="play-item">
        <Item>
          <Title>{value}</Title>
          <ButtonBox>
            <Button>Play</Button>
          </ButtonBox>
        </Item>
      </Card>
    )
  }
}
