const theme = {
  main: '#514890',
  background: 'white',
  baseRadius: '8px',
  largeRadius: '20px',
  dark: '#323232',
  shadow: '#9a9a9a',
  border: '#dedede',
  error: '#ff2a51',
  success: '#4eaa4e'
}

export default theme
