import * as React from 'react'
import { Router, Route, Switch, Redirect } from 'react-router-dom'
import HomePage from './page/HomePage'
import QuestionsPage from './page/QuestionsPage'
import QuestionFormPage from './page/QuestionFormPage'
import UploadFormPage from './page/UploadFormPage'
import QuizPage from './page/QuizPage'
import LoginPage from './page/LoginPage'
import NotFoundPage from './page/NotFoundPage'
import Nav from './component/Nav/Nav'
import Container from './component/Container/Container'
import ThemeProvider from './component/ThemeProvider/ThemeProvider'
import history from './history'
import { GlobalStyle } from './App.styles'

export default class App extends React.Component {
  render() {
    return (
      <ThemeProvider>
        <div>
          <Router history={history}>
            <div>
              <Nav />
              <Container>
                <Switch>
                  <Redirect exact from="/" to="home" />
                  <Route exact path="/home" component={HomePage} />
                  <Route exact path="/questions" component={QuestionsPage} />
                  <Route path="/questions/form" component={QuestionFormPage} />
                  <Route path="/questions/upload" component={UploadFormPage} />
                  <Route path="/quiz" component={QuizPage} />
                  <Route path="/login" component={LoginPage} />
                  <Route component={NotFoundPage} />
                </Switch>
              </Container>
            </div>
          </Router>
          <GlobalStyle />
        </div>
      </ThemeProvider>
    )
  }
}
