import { createGlobalStyle } from 'styled-components'
import reset from 'styled-reset'

export const GlobalStyle = createGlobalStyle`
  ${reset}
  
  * {
    box-sizing: border-box;
    outline: none;
    -webkit-tap-highlight-color: transparent;
  }
  
  body {
    font-family: 'Nunito', sans-serif;
    color: black;
    font-size: 15px;
  }
  
  a {
    text-decoration: none;
    color: inherit;
  }
  
  input {
    //TO REMOVE IOS DEFAULTS
    appearance: none;
    box-shadow: none !important;
  }
  
  button {
    padding: 0;
    margin: 0;
  }
`
