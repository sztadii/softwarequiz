import axios from 'axios'
import history from '../history'
import { setToken } from '../utils/authUtils'
import errorCodes from '../config/errorCodes'

axios.interceptors.response.use(null, error => {
  if (error.response.status === errorCodes.unauthorized) {
    history.push('/login')
  }
  throw new Error(error)
})

setToken(axios)

export default axios
