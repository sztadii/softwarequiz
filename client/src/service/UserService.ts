import service from './Service'
import { User } from '../type/User'
import { setToken, saveToAuthStorage } from '../utils/authUtils'

export async function authenticateUser(user: User) {
  try {
    const res = await service.post('/api/user/authenticate', user)
    saveToAuthStorage(res.data)
    setToken(service)
  } catch (e) {
    throw Error(e)
  }
}
