import service from './Service'
import { Question } from '../type/Question'

export async function fetchQuizByType(type: string): Promise<Question[]> {
  const { data } = await service.get('/api/quiz', { params: { type } })
  return data
}
