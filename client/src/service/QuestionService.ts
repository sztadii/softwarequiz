import service from './Service'
import { AxiosResponse } from 'axios'
import { Question, QuestionDocument } from '../type/Question'

export async function fetchAllQuestion(): Promise<Question[]> {
  const res: AxiosResponse<QuestionDocument> = await service.get('/api/questions')
  return res.data.docs
}

export async function fetchAllQuestionByType(type: string): Promise<Question[]> {
  const res: AxiosResponse<QuestionDocument> = await service.get(`/api/questions/type/${type}`)
  return res.data.docs
}

export async function fetchAllAvailableTypes(): Promise<string[]> {
  const res: AxiosResponse<string[]> = await service.get('/api/questions/types/available')
  return res.data
}

export async function fetchAllTypes(): Promise<string[]> {
  const res: AxiosResponse<string[]> = await service.get('/api/questions/types/all')
  return res.data
}

export async function createQuestion(question: Question) {
  return service.post('/api/questions', question)
}

export async function uploadQuestions(files: FormData): Promise<Question[]> {
  const res = await service.post('/api/questions/upload', files)
  return res.data
}

export async function updateQuestion(question: Question) {
  return service.put('/api/questions', question)
}

export async function getQuestionById(id: string): Promise<Question> {
  const res: AxiosResponse<Question> = await service.get(`/api/questions/${id}`)
  return res.data
}

export async function deleteQuestion(_id: string) {
  return service.delete(`/api/questions/${_id}`)
}

export async function deleteAllQuestions() {
  return service.delete('/api/questions')
}

export async function disApproveAllQuestions() {
  return service.put('/api/questions/disapprove-all')
}

export async function approveAllQuestions() {
  return service.put('/api/questions/approve-all')
}
