import * as React from 'react'
import QuestionForm from '../component/QuestionForm/QuestionForm'

export default class QuestionFormPage extends React.Component {
  render() {
    return <QuestionForm />
  }
}
