import * as React from 'react'
import LoginForm from '../component/LoginForm/LoginForm'

export default class LoginPage extends React.Component {
  render() {
    return <LoginForm />
  }
}
