import * as React from 'react'
import { inject, observer } from 'mobx-react'
import QuestionStore from '../store/QuestionStore'
import QuizList from '../component/QuizList/QuizList'
import Title from '../component/Title/Title'
import EmptyMessage from '../component/EmptyMessage/EmptyMessage'

interface HomePageProps {
  store?: QuestionStore
}

@inject('store')
@observer
export default class HomePage extends React.Component<HomePageProps> {
  async componentDidMount() {
    const { fetchAllAvailableTypes } = this.props.store
    await fetchAllAvailableTypes()
  }

  render() {
    const { types } = this.props.store

    if (!types.length) return <EmptyMessage />

    return (
      <div>
        <Title>Choose quiz</Title>
        <QuizList types={types} />
      </div>
    )
  }
}
