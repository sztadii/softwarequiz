import * as React from 'react'
import { inject, observer } from 'mobx-react'
import QuestionStore from '../store/QuestionStore'
import QuizPlayer from '../component/QuizPlayer/QuizPlayer'
import Title from '../component/Title/Title'
import getSearchParam from '../utils/getSearchParam'

interface QuizPageProps {
  store?: QuestionStore
  location: any
}

@inject('store')
@observer
export default class QuizPage extends React.Component<QuizPageProps> {
  async componentDidMount() {
    const { fetchQuizByType, startQuiz } = this.props.store
    startQuiz()
    const type = this.getQuizType()
    await fetchQuizByType(type)
  }

  componentWillUnmount(): void {
    const { finishQuiz } = this.props.store
    finishQuiz()
  }

  getQuizType = () => getSearchParam('type')

  render() {
    const { quiz, finishQuiz } = this.props.store
    const type = this.getQuizType()
    return (
      <div>
        <Title>{type} quiz</Title>
        <QuizPlayer questions={quiz} onFinish={finishQuiz} />
      </div>
    )
  }
}
