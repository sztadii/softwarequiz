import * as React from 'react'
import UploadForm from '../component/UploadForm/UploadForm'

export default class QuestionFormPage extends React.Component {
  render() {
    return <UploadForm />
  }
}
