import * as React from 'react'
import { inject, observer } from 'mobx-react'
import QuestionStore from '../store/QuestionStore'
import QuestionsList from '../component/QuestionsList/QuestionsList'
import Title from '../component/Title/Title'
import Button from '../component/Button/Button'
import Line from '../component/Line/Line'
import EmptyMessage from '../component/EmptyMessage/EmptyMessage'
import Space from '../component/Space/Space'
import { isAdmin } from '../utils/authUtils'

interface QuestionsPageProps {
  store?: QuestionStore
}

interface QuestionsPageState {
  activeType: string
}

@inject('store')
@observer
export default class QuestionsPage extends React.Component<QuestionsPageProps, QuestionsPageState> {
  state: QuestionsPageState = {
    activeType: null
  }

  async componentDidMount() {
    const { fetchAllQuestions, fetchAllTypes } = this.props.store
    await fetchAllQuestions()
    await fetchAllTypes()
  }

  handleQuestionTypeChange = async (type: string) => {
    const { fetchAllQuestionsByType, fetchAllQuestions } = this.props.store
    const { activeType } = this.state

    if (type === activeType) {
      await fetchAllQuestions()
      this.setState({ activeType: null })
    } else {
      await fetchAllQuestionsByType(type)
      this.setState({ activeType: type })
    }
  }

  handleDeleteAll = async () => {
    const { deleteAllQuestions } = this.props.store
    await deleteAllQuestions()
  }

  handleDisApproveAll = async () => {
    const { disApproveAllQuestions } = this.props.store
    await disApproveAllQuestions()
  }

  handleApproveAll = async () => {
    const { approveAllQuestions } = this.props.store
    await approveAllQuestions()
  }

  render() {
    const { questions, types } = this.props.store
    const { activeType } = this.state

    if (!questions.length) return <EmptyMessage />

    return (
      <div>
        <Title>All questions</Title>
        {!!types.length && (
          <Space>
            {types.map(type => {
              const isActiveClass = type === activeType ? 'is-active' : ''
              return (
                <Button
                  data-testid={`type-button-${type}`}
                  key={type}
                  className={isActiveClass}
                  onClick={() => this.handleQuestionTypeChange(type)}
                >
                  {type}
                </Button>
              )
            })}
            <Line />
          </Space>
        )}
        <QuestionsList questions={questions} />

        {isAdmin() && !!questions.length && (
          <div>
            <Line />
            <Space>
              <Button onClick={this.handleDeleteAll} data-testid="delete-all-button">
                Delete all
              </Button>
              <Button onClick={this.handleDisApproveAll} data-testid="disapprove-all-button">
                Disapprove all
              </Button>
              <Button onClick={this.handleApproveAll} data-testid="approve-all-button">
                Approve all
              </Button>
            </Space>
          </div>
        )}
      </div>
    )
  }
}
