import * as React from 'react'
import { Link } from 'react-router-dom'
import Title from '../component/Title/Title'
import Button from '../component/Button/Button'

export default class NotFoundPage extends React.Component {
  render() {
    return (
      <div>
        <Title>Page not found</Title>
        <Link to="/">
          <Button>Go to Home</Button>
        </Link>
      </div>
    )
  }
}
