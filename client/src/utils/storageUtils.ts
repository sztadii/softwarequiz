export function getFromStorage(key: string): any {
  const object = localStorage.getItem(key)
  return JSON.parse(object)
}

export function setToStorage(key: string, item: any): any {
  const object = JSON.stringify(item)
  localStorage.setItem(key, object)
}
