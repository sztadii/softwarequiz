import { AxiosInstance } from 'axios'
import { setToStorage, getFromStorage } from './storageUtils'

const authStorageKey = 'authStorage'

export function setToken(httpInstance: AxiosInstance): void {
  httpInstance.defaults.headers.common['Authorization'] = `Bearer ${getTokenFromAuthStorage()}`
}

interface AuthStorage {
  token: string
  email: string
  role: string
}

export function saveToAuthStorage(data: AuthStorage): void {
  setToStorage(authStorageKey, data)
}

export function getTokenFromAuthStorage(): string {
  const data = getAuthData()
  return data && data.token
}

export function isAdmin(): boolean {
  const data = getAuthData()
  return !!data && data.role === 'admin'
}

function getAuthData(): AuthStorage {
  return getFromStorage(authStorageKey)
}
