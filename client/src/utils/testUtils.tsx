import * as React from 'react'
import { fireEvent } from 'react-testing-library'
import { Provider } from 'mobx-react'
import { BrowserRouter } from 'react-router-dom'
import { render } from 'react-testing-library'
import QuestionStore from '../store/QuestionStore'

export function fireInputUpdate(input: HTMLInputElement, value: string) {
  input.value = value
  fireEvent.change(input)
}

export function renderWithRouterAndStore(component: any) {
  const questionStore = new QuestionStore()
  return render(
    <Provider store={questionStore}>
      <BrowserRouter>{component}</BrowserRouter>
    </Provider>
  )
}
