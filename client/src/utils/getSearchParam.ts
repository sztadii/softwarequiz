export default function getSearchParam(key: string): string {
  return new URLSearchParams(location.search).get(key)
}
