export default function decodeCorrectAnswer(text: string): string {
  return Buffer.from(text, 'hex').toString('ascii')
}
