import { observable, action } from 'mobx'
import { Question } from '../type/Question'
import * as questionService from '../service/QuestionService'
import * as quizService from '../service/QuizService'
import decodeCorrectAnswer from '../utils/decodeCorrectAnswer'

export default class QuestionStore {
  @observable
  questions: Question[] = []

  @observable
  types: string[] = []

  @observable
  quiz: Question[] = []

  @observable
  isActiveQuiz: boolean = false

  @action
  fetchAllQuestions = async () => {
    this.questions = await questionService.fetchAllQuestion()
  }

  @action
  fetchAllQuestionsByType = async (type: string) => {
    this.questions = await questionService.fetchAllQuestionByType(type)
  }

  @action
  fetchAllAvailableTypes = async () => {
    this.types = await questionService.fetchAllAvailableTypes()
  }

  @action
  fetchAllTypes = async () => {
    this.types = await questionService.fetchAllTypes()
  }

  @action
  fetchQuizByType = async (type: string) => {
    const questions = await quizService.fetchQuizByType(type)
    this.quiz = questions.map(q => ({ ...q, correctAnswer: decodeCorrectAnswer(q.correctAnswer) }))
  }

  @action
  deleteQuestion = async (_id: string) => {
    await questionService.deleteQuestion(_id)
    this.questions = this.questions.filter(q => q._id !== _id)
  }

  @action
  deleteAllQuestions = async () => {
    await questionService.deleteAllQuestions()
    this.questions = []
  }

  @action
  disApproveAllQuestions = async () => {
    await questionService.disApproveAllQuestions()
    this.questions = this.questions.map(q => ({ ...q, isApproved: false }))
  }

  @action
  approveAllQuestions = async () => {
    await questionService.approveAllQuestions()
    this.questions = this.questions.map(q => ({ ...q, isApproved: true }))
  }

  @action
  startQuiz = () => {
    this.isActiveQuiz = true
  }

  @action
  finishQuiz = () => {
    this.quiz = []
    this.isActiveQuiz = false
  }
}
