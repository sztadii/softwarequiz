import { Question } from './Question'

export interface QuizQuestion extends Question {
  chosenAnswer?: string
}
