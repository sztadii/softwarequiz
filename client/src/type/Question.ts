export interface QuestionDocument {
  docs: Question[]
  limit: number
  offset: number
  page: number
  pages: number
  total: number
}

export interface Question {
  _id?: string
  type: string
  name: string
  answers: string[]
  correctAnswer: string
  isApproved?: boolean
}
