import { createUser, removeAllUsers } from '../service/UserService'
import defaultRoles from '../config/defaultRoles'

export default async function seedData() {
  const admin = {
    email: process.env.CYPRESS_ADMIN_EMAIL,
    password: process.env.CYPRESS_ADMIN_PASSWORD,
    role: defaultRoles.admin
  }
  await removeAllUsers()
  await createUser(admin)
}
