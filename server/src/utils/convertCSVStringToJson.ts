export default function convertCSVStringToJson(csv: string) {
  const separator = csv.includes('\r\n') ? '\r\n' : '\n'
  const [head, ...tail] = csv.split(separator)
  const keys = head.split(',').map(k => k.trim())
  const rows = tail.map(el => el.split(','))
  return rows.map(row => {
    let obj = {}
    row.forEach((value, i) => {
      obj[keys[i]] = value
    })
    return obj
  })
}
