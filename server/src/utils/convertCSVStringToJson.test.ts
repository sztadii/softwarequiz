import convertCSVStringToJson from './convertCSVStringToJson'

test('convertCSVStringToJson works properly', () => {
  const csvString =
    'type,name,answer0,answer1,answer2,answer3,correctAnswer\r\nJS,What is this?,I do not know,Who cares?,Hahaha,Lol,Lol\r\nPHP,Hmm?,Ohh,Ohh2,Ohh3,Ohh4,Ohh'
  const expectedArray = [
    {
      type: 'JS',
      name: 'What is this?',
      answer0: 'I do not know',
      answer1: 'Who cares?',
      answer2: 'Hahaha',
      answer3: 'Lol',
      correctAnswer: 'Lol'
    },
    {
      type: 'PHP',
      name: 'Hmm?',
      answer0: 'Ohh',
      answer1: 'Ohh2',
      answer2: 'Ohh3',
      answer3: 'Ohh4',
      correctAnswer: 'Ohh'
    }
  ]

  expect(convertCSVStringToJson(csvString)).toEqual(expectedArray)
})
