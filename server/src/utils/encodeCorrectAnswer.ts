export default function encodeCorrectAnswer(text: string) {
  return Buffer.from(text).toString('hex')
}
