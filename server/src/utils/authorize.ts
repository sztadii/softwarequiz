import { Request, Response, NextFunction } from 'express'
import * as expressJwt from 'express-jwt'
import errorCodes from '../config/errorCodes'
import defaultRoles from '../config/defaultRoles'
import { IUser } from '../type/IUser'

interface RequestJWT extends Request {
  user: IUser
}

export default function authorize(roles: string[] = [defaultRoles.admin]) {
  return [
    expressJwt({ secret: process.env.JWT_KEY }),

    (req: RequestJWT, res: Response, next: NextFunction) => {
      if (roles.length && !roles.includes(req.user.role)) {
        return res.status(errorCodes.unauthorized).json({ message: 'Unauthorized' })
      }
      next()
    }
  ]
}
