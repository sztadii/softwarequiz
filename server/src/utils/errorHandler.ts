import { Response, Request } from 'express'

export default function errorHandler(err: Error, req: Request, res: Response) {
  if (typeof err === 'string') {
    res.status(400).json({ message: err })
  }

  if (err.name === 'UnauthorizedError' || err.message === 'UnauthorizedError') {
    res.status(401).json({ message: 'Unauthorized' })
  } else {
    res.status(500).json({ message: err.message })
  }
}
