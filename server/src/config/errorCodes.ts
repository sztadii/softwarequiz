export default {
  ok: 200,
  unprocessableEntity: 422,
  authenticated: 400,
  unauthorized: 401
}
