import * as express from 'express'
import * as quizService from '../service/QuizService'
const router = express.Router()

router.get('/', async (req, res, next) => {
  try {
    const { type } = req.query
    res.json(await quizService.getQuizQuestions(type))
  } catch (e) {
    next(e)
  }
})

export default router
