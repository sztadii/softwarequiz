import * as express from 'express'
import * as multer from 'multer'
import authorize from '../utils/authorize'
import * as questionService from '../service/QuestionService'
import errorCodes from '../config/errorCodes'

const router = express.Router()
const upload = multer()

router.get('/', async (req, res, next) => {
  try {
    const { limit, page } = req.query
    res.json(await questionService.getAllQuestions(limit, page))
  } catch (e) {
    next(e)
  }
})

router.post('/', async (req, res, next) => {
  try {
    res.json(await questionService.createQuestion(req.body))
  } catch (e) {
    next(e)
  }
})

router.put('/', authorize(), async (req, res) => {
  try {
    res.json(await questionService.updateQuestion(req.body))
  } catch (e) {
    res.status(errorCodes.unprocessableEntity).send(e.message)
  }
})

// TODO Below endpoint is unauthorized, because during e2e we will need it. Please fix it later with using node env
router.delete('/', async (req, res, next) => {
  try {
    res.json(await questionService.deleteAllQuestions())
  } catch (e) {
    next(e)
  }
})

router.put('/disapprove-all', authorize(), async (req, res, next) => {
  try {
    res.json(await questionService.disApproveAllQuestions())
  } catch (e) {
    next(e)
  }
})

router.put('/approve-all', authorize(), async (req, res, next) => {
  try {
    res.json(await questionService.approveAllQuestions())
  } catch (e) {
    next(e)
  }
})

router.get('/:id', authorize(), async (req, res, next) => {
  try {
    res.json(await questionService.getQuestionById(req.params.id))
  } catch (e) {
    next(e)
  }
})

router.delete('/:id', authorize(), async (req, res, next) => {
  try {
    res.json(await questionService.deleteQuestionById(req.params.id))
  } catch (e) {
    next(e)
  }
})

router.get('/type/:type', async (req, res, next) => {
  try {
    const { type } = req.params
    const { limit, page } = req.query
    res.json(await questionService.getQuestionsByType(type, limit, page))
  } catch (e) {
    next(e)
  }
})

router.get('/types/all', async (req, res, next) => {
  try {
    res.json(await questionService.getAllQuestionTypes())
  } catch (e) {
    next(e)
  }
})

router.get('/types/available', async (req, res, next) => {
  try {
    res.json(await questionService.getAllAvailableQuestionTypes())
  } catch (e) {
    next(e)
  }
})

router.post('/upload', authorize(), upload.single('files'), async (req, res, next) => {
  try {
    res.json(await questionService.uploadQuestions(req.file))
  } catch (e) {
    next(e)
  }
})

export default router
