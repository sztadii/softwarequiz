import * as express from 'express'
import * as userService from '../service/UserService'
const router = express.Router()

router.post('/authenticate', async (req, res, next) => {
  try {
    const user = await userService.authenticateUser(req.body)
    res.json(user)
  } catch (e) {
    next(e)
  }
})

router.post('/register', async (req, res, next) => {
  try {
    res.json(await userService.createUser(req.body))
  } catch (e) {
    next(e)
  }
})

export default router
