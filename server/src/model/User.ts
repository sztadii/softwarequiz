import { Schema, model, PaginateModel, Document } from 'mongoose'
import * as mongoosePaginate from 'mongoose-paginate'
import { IUser } from '../type/IUser'

interface User extends IUser, Document {}
interface UserModel<T extends Document> extends PaginateModel<T> {}

const UserSchema: Schema = new Schema(
  {
    email: {
      type: String,
      required: true,
      unique: true
    },
    password: {
      type: String,
      required: true
    },
    role: {
      type: String,
      required: true
    }
  },
  {
    timestamps: {
      createdAt: true,
      updatedAt: true
    }
  }
)

UserSchema.plugin(mongoosePaginate)

const UserModel: UserModel<User> = model<User>('User', UserSchema) as UserModel<User>

export default UserModel
