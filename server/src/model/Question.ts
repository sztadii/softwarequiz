import { Schema, model, PaginateModel, Document } from 'mongoose'
import * as mongoosePaginate from 'mongoose-paginate'
import { IQuestion } from '../type/IQuestion'

interface Question extends IQuestion, Document {}
interface QuestionModel<T extends Document> extends PaginateModel<T> {}

const QuestionSchema: Schema = new Schema(
  {
    type: {
      type: String,
      required: true
    },
    name: {
      type: String,
      required: true,
      unique: true
    },
    answers: {
      type: [String],
      required: true
    },
    correctAnswer: {
      type: String,
      required: true
    },
    isApproved: {
      type: Boolean,
      default: false
    }
  },
  {
    timestamps: {
      createdAt: true,
      updatedAt: true
    }
  }
)

QuestionSchema.plugin(mongoosePaginate)

const QuestionModel: QuestionModel<Question> = model<Question>(
  'Question',
  QuestionSchema
) as QuestionModel<Question>

export default QuestionModel
