import * as express from 'express'
import * as bodyParser from 'body-parser'
import questions from './controller/QuestionController'
import quiz from './controller/QuizController'
import user from './controller/UserController'
import errorHandler from './utils/errorHandler'
import seedData from './utils/seedData'
const app = express()

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use('/api/questions', questions)
app.use('/api/quiz', quiz)
app.use('/api/user', user)

app.use(errorHandler)

seedData()
  .then(() => console.log('Successful seed data'))
  .catch(e => console.log('Error during seed data', e))

export default app
