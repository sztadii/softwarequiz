export interface IQuestion {
  type: string
  name: string
  answers: string[]
  correctAnswer: string
  isApproved: boolean
}

export interface ICSVQuestion {
  type: string
  name: string
  answer0: string
  answer1: string
  answer2: string
  answer3: string
  correctAnswer: string
}
