import Question from '../model/Question'
import convertCSVStringToJson from '../utils/convertCSVStringToJson'
import { ICSVQuestion } from '../type/IQuestion'

export function getAllQuestions(limit = 10, page = 1) {
  const options = {
    sort: {
      createdAt: -1
    },
    limit,
    page
  }
  return Question.paginate({}, options)
}

export function getQuestionById(_id) {
  return Question.findById(_id)
}

export function createQuestion(question) {
  return new Question(question).save()
}

export function uploadQuestions(file) {
  const list = convertCSVStringToJson(file.buffer.toString())
  const questions = list.map((e: ICSVQuestion) => {
    return {
      type: e.type,
      name: e.name,
      answers: [e.answer0, e.answer1, e.answer2, e.answer3],
      correctAnswer: e.correctAnswer
    }
  })
  return Question.create(questions)
}

export function updateQuestion(question) {
  return Question.updateOne({ _id: question._id }, { $set: { ...question } })
}

export function deleteQuestionById(_id) {
  return Question.remove({ _id })
}

export function deleteAllQuestions() {
  return Question.remove({})
}

export function disApproveAllQuestions() {
  return Question.updateMany({}, { $set: { isApproved: false } })
}

export function approveAllQuestions() {
  return Question.updateMany({}, { $set: { isApproved: true } })
}

export function getQuestionsByType(type, limit = 10, page = 1) {
  const options = {
    sort: {
      createdAt: -1
    },
    limit,
    page
  }
  return Question.paginate({ type }, options)
}

export function getAllAvailableQuestionTypes(): Promise<string[]> {
  return Question.find({ isApproved: true })
    .distinct('type')
    .exec()
}

export function getAllQuestionTypes(): Promise<string[]> {
  return Question.find({})
    .distinct('type')
    .exec()
}
