import Question from '../model/Question'
import { IQuestion } from '../type/IQuestion'
import { shuffle } from 'lodash'
import encodeCorrectAnswer from '../utils/encodeCorrectAnswer'

export async function getQuizQuestions(type: string): Promise<Array<IQuestion>> {
  const mainCondition = { isApproved: true }
  const questions = await Question.find(!!type ? { type, ...mainCondition } : mainCondition)
  return shuffle(questions)
    .slice(0, 10)
    .map(q => {
      return {
        _id: q._id,
        type: q.type,
        name: q.name,
        answers: shuffle(q.answers),
        correctAnswer: encodeCorrectAnswer(q.correctAnswer),
        isApproved: q.isApproved
      }
    })
}
