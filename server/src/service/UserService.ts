import * as jwt from 'jsonwebtoken'
import User from '../model/User'
import { IUser } from '../type/IUser'

export async function authenticateUser(user: IUser) {
  const authUser = await User.findOne({ email: user.email, password: user.password }).exec()
  if (authUser) {
    return {
      token: jwt.sign({ sub: authUser._id, role: authUser.role }, process.env.JWT_KEY),
      email: authUser.email,
      role: authUser.role
    }
  } else {
    throw new Error('UnauthorizedError')
  }
}

export function createUser(user) {
  return new User(user).save()
}

export function removeAllUsers() {
  return User.remove({})
}
