import app from './src/app'
import { DB_URI, SERVER_PORT } from './src/config/defaults'
import * as mongoose from 'mongoose'

mongoose.connect(
  DB_URI,
  { useMongoClient: true }
)

app.listen(SERVER_PORT, () => {
  console.log(`Running on port ${SERVER_PORT}`)
  console.log('--------------------')
})
